import pymongo
from pymongo import MongoClient
import json
import logging
logging.basicConfig(level=logging.DEBUG,
                    format='%(asctime)s %(filename)s[line:%(lineno)d] %(levelname)s %(message)s',
                    datefmt='%a, %d %b %Y %H:%M:%S',
                    filename='./sats.log',
                    filemode='w')

client=MongoClient('127.0.0.1',27017)
db_flysat = client.flysat
db_lyngsat = client.lyngsat

flysat_sat_list = db_flysat.sats.find({})
lyngsat_sat_list = db_lyngsat.sats.find({})

def f(sat):
    del sat['_id']
    return sat

flysat_sat_list = list(flysat_sat_list[:])
#flysat_sat_list = map(f,flysat_sat_list)
lyngsat_sat_list = list(lyngsat_sat_list[:])
#lyngsat_sat_list = map(f,lyngsat_sat_list)



logging.info('------------------ Analyse bingo start ------------------')
bingo_num = 0
for s1 in flysat_sat_list:
    for s2 in lyngsat_sat_list:
        if(s1['name'] == s2['name'] and s1['orbit']==s2['orbit']):
            bingo_num += 1
            del s1['_id']
            del s2['_id']
            logging.info(s1['orbit'] + "-"+ s1['name'] + " bingo , flysat : " + json.dumps(s1))
            logging.info(s1['orbit'] + "-"+ s1['name'] + " bingo , lyngsat : " + json.dumps(s2))
logging.info('------------------ Analyse bingo end --------------------')

logging.info('------------------ summary start --------------------')
logging.info('bingo number : ' + str(bingo_num))
logging.info('flysat sat number : ' + str(len(flysat_sat_list)) + " , owned particularly number : " + str(len(flysat_sat_list)-bingo_num))
logging.info('lyngsat sat number : ' + str(len(lyngsat_sat_list)) + " , owned particularly number : " + str(len(lyngsat_sat_list)-bingo_num))
logging.info('------------------ summary end ----------------------')

