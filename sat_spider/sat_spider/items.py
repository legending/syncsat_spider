# -*- coding: utf-8 -*-

# Define here the models for your scraped items
#
# See documentation in:
# http://doc.scrapy.org/en/latest/topics/items.html

import scrapy

class SatItem(scrapy.Item):
    # define the fields for your item here like:
    name = scrapy.Field()  #db field
    url = scrapy.Field()
    orbit = scrapy.Field() #db field
    band = scrapy.Field()  #db field
    area = scrapy.Field()  #db field
    time = scrapy.Field()  #db field


class TpItem(scrapy.Item):
    sat_orbit = scrapy.Field()
    sat_link = scrapy.Field()
    pkg_link = scrapy.Field()
    desc = scrapy.Field()             #db field
    pkg_name = scrapy.Field()
    pkg_web = scrapy.Field()
    freq = scrapy.Field()             #db field , 11738 V
    pol = scrapy.Field()              #db field
    mode = scrapy.Field()             #db field
    sym = scrapy.Field()              #db field , SR 40000
    fec = scrapy.Field()              #db field , FEC 2/3
    time = scrapy.Field()             #db field
    footprints_text = scrapy.Field()  #db field , tp 13
    sat = scrapy.Field()              #db field , fk
    pkg = scrapy.Field()              #db field , fk
    band = scrapy.Field()             #db field


class FootPrintsItem(scrapy.Item):
    sat_orbit = scrapy.Field()
    freq = scrapy.Field()
    pol = scrapy.Field()
    image_urls = scrapy.Field()
    images = scrapy.Field()


class ChanItem(scrapy.Item):
    sat_orbit = scrapy.Field()
    sat_name = scrapy.Field()
    sat_band = scrapy.Field()
    sat_area = scrapy.Field()
    sat_update_time = scrapy.Field()
    sat_link = scrapy.Field()

    pkg_link = scrapy.Field()
    pkg_name = scrapy.Field()
    pkg_band = scrapy.Field()
    pkg_web = scrapy.Field()
    pkg_update_time = scrapy.Field()
    pkg_area = scrapy.Field()

    tp_freq = scrapy.Field()
    tp_pol = scrapy.Field()
    tp_fec = scrapy.Field()
    tp_sym = scrapy.Field()
    tp_mode = scrapy.Field()
    tp_footprints_text = scrapy.Field()

    name = scrapy.Field()           #db field
    web = scrapy.Field()
    vpid = scrapy.Field()           #db field
    apid = scrapy.Field()           #db field
    sid = scrapy.Field()            #db field
    desc = scrapy.Field()           #db field
    chantype = scrapy.Field()       #db field
    group = scrapy.Field()          #db field
    logo_url = scrapy.Field()
    chno = scrapy.Field()
    system = scrapy.Field()
    tp = scrapy.Field()             #db field
    sat = scrapy.Field()            #db field
    pkgs = scrapy.Field()           #db field
    encryption = scrapy.Field()     #db field


class PkgItem(scrapy.Item):
    name = scrapy.Field()         #dbfield
    url = scrapy.Field()
    sat_name = scrapy.Field()
    sat_orbit = scrapy.Field()
    sat_url = scrapy.Field()
    band = scrapy.Field()         #dbfield
    time = scrapy.Field()         #dbfield
    web = scrapy.Field()          #dbfield
    sat = scrapy.Field()          #dbfield fk
    area = scrapy.Field()         #dbfield