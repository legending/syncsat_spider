# -*- coding: utf-8 -*-
import os

BOT_NAME = 'sat_spider'
SPIDER_MODULES = ['sat_spider.spiders']
NEWSPIDER_MODULE = 'sat_spider.spiders'
USER_AGENT = 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/59.0.3071.115 Safari/537.36'
ROBOTSTXT_OBEY = False
ITEM_PIPELINES = {
    # 'scrapy.pipelines.images.ImagesPipeline': 1,
    'sat_spider.pipelines.SatSpiderPipeline': 300
}
MONGO_URI = 'mongodb://localhost:27017'
MONGO_DATABASE = 'syncsat'
LOG_FILE = "run.log"
LOG_LEVEL = 'INFO'
#DOWNLOAD_DELAY = 0.25

#--------- flysat setting ----------
IMAGES_STORE = './images/'
#IMAGES_STORE = os.path.join(os.path.dirname(__file__),"../img/")

