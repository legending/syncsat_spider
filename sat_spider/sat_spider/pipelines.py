# -*- coding: utf-8 -*-

# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: http://doc.scrapy.org/en/latest/topics/item-pipeline.html

import pymongo
from sat_spider.items import SatItem, TpItem, PkgItem, ChanItem, FootPrintsItem
import logging


class SatSpiderPipeline(object):
    collection_sat = 'sats'
    collection_tp = 'tps'
    collection_package = 'packages'
    collection_chan = 'channels'
    collection_footprints = 'footprints'

    def __init__(self, mongo_uri, mongo_db):
        self.mongo_uri = mongo_uri
        self.mongo_db = mongo_db

    @classmethod
    def from_crawler(cls, crawler):
        return cls(
            mongo_uri=crawler.settings.get('MONGO_URI'),
            mongo_db=crawler.settings.get('MONGO_DATABASE', 'syncsat')
        )

    def open_spider(self, spider):
        self.client = pymongo.MongoClient(self.mongo_uri)
        self.db = self.client[self.mongo_db]

    def close_spider(self, spider):
        self.client.close()

    def save_sat(self, item, spider):
        if 'orbit' not in item.keys() or item['orbit'] is None  or len(item['orbit']) == 0:
            print('error: ' + str(item))
            return
        if 'name' not in item.keys() or item['name'] is None  or len(item['name']) == 0:
            print('error: ' + str(item))
            return
        '''
        if 'band' not in item.keys() or item['band'] is None or len(item['band']) == 0:
            print('error: ' + str(item))
            return
        '''
        if 'area' not in item.keys() or item['area'] is None or len(item['area']) == 0:
            print('error: ' + str(item))
            return

        if 'band' in item.keys() and item['band'] is not None and len(item['band']) > 0:
            band = item['band']
        else:
            band = ''
        if 'time' in item.keys() and item['time'] is not None and len(item['time']) > 0:
            time = item['time']
        else:
            time = ''
        sat = {}
        sat['name'] = item['name']
        sat['orbit'] = item['orbit']
        sat['band'] = band
        sat['area'] = item['area']
        sat['time'] = time
        self.db[self.collection_sat].update({'orbit': sat['orbit'],'name': sat['name']}, {'$set': sat}, upsert=True) #对于upsert(默认为false)：如果upsert=true，如果query找到了符合条件的行，则修改这些行，如果没有找到，则追加一行符合query和obj的行。如果upsert为false，找不到时，不追加.对于multi(默认为false): 如果multi=true，则修改所有符合条件的行，否则只修改第一条符合条件的行。

    def save_pkg(self, item, spider):
        if 'sat_orbit' not in item.keys() or item['sat_orbit'] is None or len(item['sat_orbit']) == 0:
            print('error: ' + str(item))
            return
        if 'sat_name' not in item.keys() or item['sat_name'] is None or len(item['sat_name']) == 0:
            print('error: ' + str(item))
            return
        if 'name' not in item.keys() or item['name'] is None or len(item['name']) == 0:
            print('error: ' + str(item))
            return
        if 'band' not in item.keys() or item['band'] is None or len(item['band']) == 0:
            print('error: ' + str(item))
            return
        '''
        if ('band' in item.keys() and item['band'] is not None and len(item['band']) > 0):
            pkg_band = item['band']
        else:
            pkg_band = ''
        '''
        if 'area' in item.keys() and item['area'] is not None and len(item['area']) > 0:
            pkg_area = item['area']
        else:
            pkg_area = ''
        if 'pkg_web' in item.keys() and item['pkg_web'] is not None and len(item['pkg_web']) > 0:
            pkg_web = item['pkg_web']
        else:
            pkg_web = ''
        if 'time' in item.keys() and item['time'] is not None and len(item['time']) > 0:
            pkg_time = item['time']
        else:
            pkg_time = ''

        sat_id = ''
        p_sat = self.db[self.collection_sat].find_one({"orbit": item['sat_orbit'],"name": item['sat_name']})
        if p_sat:
            sat_id = p_sat['_id']
        else:
            if 'sat_url' in item.keys():
                sat_link_tmp = item['sat_url']
            else:
                sat_link_tmp = ''
            if 'url' in item.keys():
                pkg_link_tmp = item['url']
            else:
                pkg_link_tmp = ''
            logging.info("save_pkg： sat " + item['sat_orbit'] + " doesnt exist in db , sat_url: " + sat_link_tmp + " , pkg_url: " + pkg_link_tmp)

            new_sat = {}
            new_sat['orbit'] = item['sat_orbit']
            new_sat['name'] = item['sat_name']
            new_sat['band'] = item['band']  # regard pkg band as sat band
            new_sat['area'] = pkg_area
            new_sat['time'] = ''

            rt = self.db[self.collection_sat].update({"name": new_sat['name'], "orbit": new_sat['orbit']},
                                                     {"$set": new_sat}, upsert=True)  # prevent insert dumplicate
            if not rt['updatedExisting']:
                sat_id = rt['upserted']
                logging.info("save_pkg： added new sat , " + new_sat['orbit'] + " , " + new_sat['name'])
            else:
                logging.info("save_pkg： update sat , " + new_sat['orbit'] + " , " + new_sat['name'])
            '''    
            rt = self.db[self.collection_sat].insert_one(new_sat)
            if rt.acknowledged:
                sat_id = rt.inserted_id
                logging.info("save_pkg： added new sat , " + item['sat_orbit'] + " , " + item['sat_name'] + " , pkg url : " + pkg_link_tmp)
            else:
                logging.info("save new sat " + item['sat_orbit'] + " failed")
                return
            '''
        pkg = {}
        pkg['name'] = item['name']
        pkg['band'] = item['band']
        pkg['area'] = pkg_area
        pkg['sat'] = sat_id
        pkg['web'] = pkg_web
        pkg['time'] = pkg_time
        self.db[self.collection_package].update({'sat': pkg['sat'], 'name': pkg['name']}, {'$set': pkg}, upsert=True)

    def save_chan(self, item, spider):
        if 'sat_orbit' not in item.keys() or item['sat_orbit'] is None  or len(item['sat_orbit']) == 0:
            print('error: ' + str(item))
            return
        if ('sat_name' not in item.keys() or item['sat_name'] is None or len(item['sat_name']) == 0) and ('pkg_name' not in item.keys() or item['pkg_name'] is None or len(item['pkg_name']) == 0):
            print('error: ' + str(item))
            return
        '''
        if ('pkg_name' not in item.keys() or item['pkg_name'] is None or len(item['pkg_name']) == 0):
            print('error: ' + str(item))
            return
        '''
        if 'tp_freq' not in item.keys() or item['tp_freq'] is None or len(item['tp_freq']) == 0:
            print('error: ' + str(item))
            return
        if 'tp_pol' not in item.keys() or item['tp_pol'] is None or len(item['tp_pol']) == 0:
            print('error: ' + str(item))
            return
        if 'name' not in item.keys() or item['name'] is None or len(item['name']) == 0:
            print('error: ' + str(item))
            return

        if ('sat_band' in item.keys()) and len(item['sat_band']) > 0:
            sat_band = item['sat_band']
        else:
            sat_band = ''
        if ('sat_area' in item.keys()) and len(item['sat_area']) > 0:
            sat_area = item['sat_area']
        else:
            sat_area = ''
        if 'sat_link' in item.keys() and len(item['sat_link']) > 0:
            sat_link = item['sat_link']
        else:
            sat_link = ''

        if ('pkg_band' in item.keys()) and len(item['pkg_band']) > 0:
            pkg_band = item['pkg_band']
        else:
            pkg_band = ''
        if ('pkg_area' in item.keys()) and len(item['pkg_area']) > 0:
            pkg_area = item['pkg_area']
        else:
            pkg_area = ''
        if ('pkg_link' in item.keys()) and len(item['sat_link']) > 0:
            pkg_link = item['pkg_link']
        else:
            pkg_link = ''

        if ('tp_fec' in item.keys()) and len(item['tp_fec']) > 0:
            tp_fec = item['tp_fec']
        else:
            tp_fec = ''
        if ('tp_sym' in item.keys()) and len(item['tp_sym']) > 0:
            tp_sym = item['tp_sym']
        else:
            tp_sym = ''
        if ('tp_mode' in item.keys()) and len(item['tp_mode']) > 0:
            tp_mode = item['tp_mode']
        else:
            tp_mode = ''
        if ('tp_desc' in item.keys()) and len(item['tp_desc']) > 0:
            tp_desc = item['tp_desc']
        else:
            tp_desc = ''
        if ('tp_footprints_text' in item.keys()) and len(item['tp_footprints_text']) > 0:
            tp_footprints_text = item['tp_footprints_text']
        else:
            tp_footprints_text = ''

        sat_id = ''
        pkg_id = ''
        tp_id = ''
        p_sat = self.db[self.collection_sat].find_one({"orbit": item['sat_orbit'], "name": item['sat_name']})
        if 'pkg_name' in item.keys() and len(item['pkg_name']) > 0:
            p_pkg = self.db[self.collection_package].find_one({"sat": sat_id, "name": item['pkg_name']})

        if 'sat_name' in item.keys() and len(item['sat_name']) > 0 and ('pkg_name' not in item.keys() or len(item['pkg_name'])==0):  # chan from satlist
            if p_sat:
                sat_id = p_sat['_id']
            else:
                logging.info("save chan : sat not found , " + item['sat_orbit'] + " failed")
                logging.info("save_chan： sat " + item['sat_orbit'] + " not found , sat_url: " + sat_link + " , pkg_url: " + pkg_link)
                new_sat = {}
                new_sat['orbit'] = item['sat_orbit']
                new_sat['name'] = item['sat_name']
                new_sat['band'] = sat_band
                new_sat['area'] = sat_area
                new_sat['time'] = ''
                rt = self.db[self.collection_sat].update({"name": new_sat['name'], "orbit": new_sat['orbit']},
                                                         {"$set": new_sat}, upsert=True)  # prevent insert dumplicate
                if not rt['updatedExisting']:
                    sat_id = rt['upserted']
                    logging.info("save_chan： added new sat , " + new_sat['orbit'] + " , " + new_sat['name'])
                else:
                    logging.info("save_chan： update sat , " + new_sat['orbit'] + " , " + new_sat['name'])
                '''
                rt = self.db[self.collection_sat].insert_one({new_sat})
                if rt.acknowledged:
                    sat_id = rt.inserted_id
                    #p_new_sat = self.db[self.collection_sat].find_one({"_id": rt['upserted']})
                    #sat_id = p_new_sat['_id']
                    logging.info("save_chan： added new sat : " + item['sat_orbit'] + " , " + item['sat_name'])
                else:
                    logging.info("save new sat " + item['sat_orbit'] + " failed")
                    return
                '''

        if 'pkg_name' in item.keys() and len(item['pkg_name']) > 0 and 'sat_name' in item.keys() and len(item['sat_name']) > 0:  # chan from pkglist
            if p_sat:
                sat_id = p_sat['_id']
            else:
                logging.info("save chan : sat not found , " + item['sat_orbit'] + " failed")
                logging.info("save_chan： sat " + item['sat_orbit'] + " not found , sat_url: " + sat_link + " , pkg_url: " + pkg_link)

                new_sat = {}
                new_sat['orbit'] = item['sat_orbit']
                new_sat['name'] = item['sat_name']
                new_sat['band'] = sat_band
                new_sat['area'] = sat_area
                new_sat['time'] = ''
                rt = self.db[self.collection_sat].update({"name": new_sat['name'], "orbit": new_sat['orbit']},
                                                         {"$set": new_sat}, upsert=True)  # prevent insert dumplicate
                if not rt['updatedExisting']:
                    sat_id = rt['upserted']
                    logging.info("save_chan： added new sat , " + new_sat['orbit'] + " , " + new_sat['name'])
                else:
                    logging.info("save_chan： update sat , " + new_sat['orbit'] + " , " + new_sat['name'])
                '''
                rt = self.db[self.collection_sat].insert_one({new_sat})
                if rt.acknowledged:
                    sat_id = rt.inserted_id
                    logging.info("save_chan： added new sat : " + item['sat_orbit'] + " , " + item['sat_name'])
                else:
                    logging.info("save new sat " + item['sat_orbit'] + " failed")
                    return
                '''

            if p_pkg:
                pkg_id = p_pkg['_id']
            else: #save new pkg
                new_pkg = {}
                new_pkg['name'] = item['pkg_name']
                new_pkg['band'] = pkg_band
                new_pkg['area'] = pkg_area
                new_pkg['sat'] = sat_id
                new_pkg['time'] = ''
                new_pkg['web'] = ''
                rt = self.db[self.collection_package].update({"name": new_pkg['name'], "sat": new_pkg['sat']},{"$set": new_pkg}, upsert=True)
                if not rt['updatedExisting']:
                    pkg_id = rt['upserted']
                    logging.info("save_chan： added new pkg , " + item['pkg_name'] + " , sat_id = " + str(new_pkg['sat']))
                else:
                    logging.info("save_chan： update pkg , " + item['pkg_name'] + " , sat_id : " + str(new_pkg['sat']))
                '''
                rt = self.db[self.collection_package].insert_one(new_pkg)
                if rt.acknowledged:
                    pkg_id = rt.inserted_id
                    logging.info("save_chan： added new pkg : " + item['pkg_name'] + " , " + item['pkg_area'])
                else:
                    logging.info("save new pkg " + item['pkg_name'] + " failed")
                    return
                '''

        p_tp = self.db[self.collection_tp].find_one({"sat": sat_id, "freq": item['tp_freq'], "pol": item['tp_pol']})
        if p_tp:
            tp_id = p_tp['_id']
        else:
            new_tp = {}
            new_tp['sat'] = sat_id
            new_tp['freq'] = item['tp_freq']
            new_tp['pol'] = item['tp_pol']
            new_tp['mode'] = tp_mode
            new_tp['sym'] = tp_sym
            new_tp['fec'] = tp_fec
            new_tp['footprints_text'] = tp_footprints_text
            new_tp['time'] = ""
            new_tp['desc'] = tp_desc
            rt = self.db[self.collection_tp].update({"sat": new_tp['sat'], "freq": new_tp['freq'], "pol": new_tp['pol']},
                                                    {"$set": new_tp}, upsert=True)
            if not rt['updatedExisting']:
                tp_id = rt['upserted']
                logging.info("save_chan： added new tp , sat_id = " + str(new_tp['sat']) + " , " + new_tp['freq'] + " , " + new_tp['pol'])
            else:
                logging.info("save_chan： update tp , sat_id = " + str(new_tp['sat']) + " , " + new_tp['freq'] + " , " + new_tp['pol'])
            '''
            rt = self.db[self.collection_tp].insert_one(new_tp)
            if rt.acknowledged and pkg_id:
                self.db[self.collection_tp].update({'sat': new_tp['sat'], 'freq': new_tp['freq'], 'pol': new_tp['pol']},
                                                        {'$addToSet': {'pkgs': pkg_id}}, upsert=True)
                tp_id = rt.inserted_id
            '''

        chan = {}
        chan['sat'] = sat_id
        chan['tp'] = tp_id
        chan['name'] = item['name']
        if 'sid' in item.keys() and item['sid'] is not None and len(item['sid']) > 0:
            chan['sid'] = item['sid']
        else:
            chan['sid'] = ''
        '''
        if ('freq' in item.keys() and item['freq'] is not None and len(item['freq']) > 0):
            chan['freq'] = item['freq']
        if ('pol' in item.keys() and item['pol'] is not None and len(item['pol']) > 0):
            chan['pol'] = item['pol']
        if('web' in item.keys() and item['web'] is not None and len(item['web']) > 0):
            chan['web'] = item['web']
        '''
        if 'vpid' in item.keys()  and item['vpid'] is not None and len(item['vpid']) > 0:
            chan['vpid'] = item['vpid']
        else:
            chan['vpid'] = ''
        if 'apid' in item.keys() and item['apid'] is not None and len(item['apid']) > 0:
            chan['apid'] = item['apid']
        else:
            chan['apid'] = ''
        '''
        if('desc' in item.keys() and item['desc'] is not None and len(item['desc']) > 0):
            chan['desc'] = item['desc']
        '''
        if 'chantype' in item.keys() and item['chantype'] is not None and len(item['chantype']) > 0:
            chan['chantype'] = item['chantype']
        else:
            chan['chantype'] = ''
        if 'group' in item.keys() and item['group'] is not None and len(item['group']) > 0:
            chan['group'] = item['group']
        else:
            chan['group'] = ''
        if 'encryption' in item.keys() and item['encryption'] is not None and len(item['encryption']) > 0:
            chan['encryption'] = item['encryption']
        else:
            chan['encryption'] = []
        '''
        if ('logo_url' in item.keys() and item['logo_url'] is not None and len(item['logo_url']) > 0):
            chan['logo_url'] = item['logo_url']
        if ('chno' in item.keys() and item['chno'] is not None and len(item['chno']) > 0):
            chan['chno'] = item['chno']
        if ('system' in item.keys() and item['system'] is not None and len(item['system']) > 0):
            chan['system'] = item['system']
        '''

        if len(chan['sid']) > 0:
            self.db[self.collection_chan].update({'sat': sat_id, "tp": tp_id, 'sid': chan['sid'], "name": chan['name']}, {'$set': chan}, upsert=True)
            if pkg_id:
                self.db[self.collection_chan].update({'sat': sat_id, "tp": tp_id, 'sid': chan['sid'], "name": chan['name']},
                                                    {'$addToSet': {'pkgs': pkg_id}}, upsert=True)
            if len(item['encryption']) > 0:
                self.db[self.collection_chan].update({'sat': sat_id, "tp": tp_id, 'sid': chan['sid'], "name": chan['name']},
                                                 {'$addToSet': {'encryption': {'$each': item['encryption']}}}, upsert=True)
        else:
            self.db[self.collection_chan].update({'sat': sat_id, "tp": tp_id, "name": chan['name']}, {'$set': chan}, upsert=True)
            if pkg_id:
                self.db[self.collection_chan].update({'sat': sat_id, "tp": tp_id, "name": chan['name']},
                                                    {'$addToSet': {'pkgs': pkg_id}}, upsert=True)
            if len(item['encryption']) > 0:
                self.db[self.collection_chan].update({'sat': sat_id, "tp": tp_id, "name": chan['name']},
                                                 {'$addToSet': {'encryption': {'$each': item['encryption']}}}, upsert=True)

    def save_tp(self, item, spider):
        if 'sat_orbit' not in item.keys() or item['sat_orbit'] is None or len(item['sat_orbit']) == 0:
            print('error: ' + str(item))
            return
        if 'freq' not in item.keys() or item['freq'] is None or len(item['freq']) == 0:
            print('error: ' + str(item))
            return
        if 'pol' not in item.keys() or item['pol'] is None or len(item['pol']) == 0:
            print('error: ' + str(item))
            return
        '''
        if ('pkg_name' not in item.keys() or item['pkg_name'] is None or len(item['pkg_name']) == 0):
            print('error: ' + str(item))
            return
        '''

        sat_id = ''
        pkg_id = ''
        sat = self.db[self.collection_sat].find_one({"orbit": item['sat_orbit']})
        if sat:
            sat_id = sat['_id']
        else:
            if 'sat_link' in item.keys():
                sat_link_tmp = item['sat_link']
            else:
                sat_link_tmp = ''
            logging.info("save_tp: sat " + item['sat_orbit'] + " doesnt exist in db , sat_link: " + sat_link_tmp)
            return
        if 'pkg_name' in item.keys() and item['pkg_name'] is not None and len(item['pkg_name']) > 0:
            pkg = self.db[self.collection_package].find_one({"sat": sat_id, 'name':item['pkg_name']})
            if (pkg):
                pkg_id = pkg['_id']
            else:
                pkg_id = ""
                if 'pkg_link' in item.keys():
                    pkg_link_tmp = item['pkg_link']
                else:
                    pkg_link_tmp = ''
                if 'sat_link' in item.keys():
                    sat_link_tmp = item['sat_link']
                else:
                    sat_link_tmp = ''
                logging.info("save_tp: pkg " + item['pkg_name']+ " doesnt exist in db , pkg_link: " + pkg_link_tmp + " , sat_link : " + sat_link_tmp)
                #return
        else:
            pkg_id = ""

        tp = {}
        #tp['sat_orbit'] = item['sat_orbit']
        tp['sat'] = sat_id
        tp['freq'] = item['freq']
        tp['pol'] = item['pol']
        # + ',' + tp['pol']
        if 'sym' in item.keys() and item['sym'] is not None and len(item['sym']) > 0:
            tp['sym'] = item['sym']
        else:
            tp['sym'] = ''
        if 'fec' in item.keys() and item['fec'] is not None and len(item['fec']) > 0:
            tp['fec'] = item['fec']
        else:
            tp['fec'] = ''
        if 'time' in item.keys() and item['time'] is not None and len(item['time']) > 0:
            tp['time'] = item['time']
        else:
            tp['time'] = ''
        if 'desc' in item.keys() and item['desc'] is not None and len(item['desc']) > 0:
            tp['desc'] = item['desc']
        else:
            tp['desc'] = ''
        # if('pkg_name' in item.keys()):
        #    tp['pkg_name'] = item['pkg_name']
        # if('pkg_web' in item.keys()):
        #    tp['pkg_web'] = item['pkg_web']
        if 'mode' in item.keys() and item['mode'] is not None and len(item['mode']) > 0:
            tp['mode'] = item['mode']
        else:
            tp['mode'] = ''
        if 'footprints_text' in item.keys() and item['footprints_text'] is not None and len(item['footprints_text']) > 0:
            tp['footprints_text'] = item['footprints_text']
        else:
            tp['footprints_text'] = ''
        # test
        '''
        if ('sat_link' in item.keys() and item['sat_link'] is not None and len(item['sat_link']) > 0):
            tp['sat_link'] = item['sat_link']
        '''
        self.db[self.collection_tp].update({'sat': tp['sat'], 'freq': tp['freq'], 'pol': tp['pol']}, {'$set': tp}, upsert=True)
        if pkg_id:
            self.db[self.collection_tp].update({'sat': tp['sat'], 'freq': tp['freq'], 'pol': tp['pol']},{'$addToSet': {'pkgs': pkg_id}}, upsert=True)

    def process_item(self, item, spider):
        if isinstance(item, SatItem):
            self.save_sat(item, spider)
        elif isinstance(item, TpItem):
            self.save_tp(item, spider)
        elif isinstance(item, PkgItem):
            self.save_pkg(item, spider)
        elif isinstance(item, ChanItem):
            self.save_chan(item, spider)
        return item

if __name__ == "__main__":
    from pymongo import MongoClient
    client = MongoClient('127.0.0.1',27017)
    db = client.test

    '''
    user = {"name":"luna","age":24}
    rt1 = db.user.insert(user)
    print(isinstance(rt1,object))                        # True
    print(isinstance(rt1,dict))                          # False
    print(rt1)                                           # 5a0e4af969456414d2b34bbf

    users = [{"name":"hhahah","age":45},{"name":"lulu","age":23}]
    rt2 = db.user.insert(users)
    print(isinstance(rt2, object))    # True
    print(isinstance(rt2, dict))      # False
    print(rt2)                        # [ObjectId('5ab33e426945647f532aecbd'), ObjectId('5ab33e426945647f532aecbe')]

    user1 = {"name":"llal","age":67}
    rt3 = db.user.save(user1)
    print(isinstance(rt3, object))    # True
    print(isinstance(rt3, dict))      # False
    print(rt3)                        # 5ab33e426945647f532aecbf

    user = {"name":"lili","age":58}
    rt = db.user.insert_one(user)
    print(isinstance(rt,object))  # True
    print(isinstance(rt, dict))   # False
    print(rt.inserted_id)         # 5ab336bc6945646f5f9ae5cb
    print(rt.acknowledged)        # True
    '''


    user = {"name":"liliss","age":99}
    rt1 = db.user.update({'name': user['name']}, {'$set': user},upsert=True)
    print(isinstance(rt1,object)) # True
    print(isinstance(rt1,dict))   # True
    print(str(rt1['upserted']))   # 5ab4a664b4be31431e5c8eb8
    print(rt1)
    #add: {'ok': 1, 'updatedExisting': False, 'n': 1, 'upserted': ObjectId('5a0d64ee46f6c906deb9db67'), 'nModified': 0}
    #update: {'nModified': 0, 'ok': 1, 'n': 1, 'updatedExisting': True}




