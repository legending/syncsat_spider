# coding: utf-8
import scrapy
import logging
from urllib import parse
from sat_spider.items import SatItem, TpItem, PkgItem, ChanItem, FootPrintsItem


class LyngsatSatSpider(scrapy.Spider):
    name = "lyngsat"
    index_url = "https://www.lyngsat.com/"

    def start_requests(self):
        print(self.index_url)
        yield scrapy.Request(url=self.index_url, callback=self.urls_parse)

    def urls_parse(self, response):
        tab = response.css('table[width="468"][border="1"][cellspacing="0"][cellpadding="2"][bgcolor="lightyellow"]')[0]
        sat_area_alist = tab.css('tr:nth-child(2) a')
        satarea_url = {}
        for a in sat_area_alist:
            if a.css("::text").extract_first().strip() == "Europe":
                satarea_url['Europe-MiddleEast-Africa'] = a.css('::attr(href)').extract_first()
                continue
            satarea_url[a.css("::text").extract_first().strip()] = a.css('::attr(href)').extract_first()
        for area,url in satarea_url.items():
            satarea_url[area] = parse.urljoin(self.index_url,url)
        #print(satarea_url)
        for area,url in satarea_url.items():
            yield response.follow(url, callback=self.satlist_parse, meta={"area":area})

        pkg_area_alist = tab.css('tr:nth-child(3) a')
        pkgarea_url = {}
        for a in pkg_area_alist:
            if a.css("::text").extract_first().strip() == "Europe":
                pkgarea_url['Europe-MiddleEast-Africa'] = a.css('::attr(href)').extract_first()
                continue
            pkgarea_url[a.css("::text").extract_first().strip()] = a.css('::attr(href)').extract_first()
        for area, url in pkgarea_url.items():
            pkgarea_url[area] = parse.urljoin(self.index_url,url)
        #print(pkgarea_url)
        for area,url in pkgarea_url.items():
            yield response.follow(url, callback=self.pkglist_parse, meta={"area": area,"req_url":url})

    def satlist_parse(self,response):
        area = response.meta['area']
        cnt = 0
        sat_color_band = {}
        color_tds = response.css('table[align="center"]')[1].css('td[bgcolor]')
        for td in color_tds:
            band_type = td.css("font::text").extract_first().strip()
            if band_type == "C band":
                sat_color_band[td.css("::attr(bgcolor)").extract_first()] = "C"
            elif band_type == "C & Ku band":
                sat_color_band[td.css("::attr(bgcolor)").extract_first()] = "C/Ku"
            elif band_type == "Ku band":
                sat_color_band[td.css("::attr(bgcolor)").extract_first()] = "Ku"
            else:  # set no data/L/S/Ka band , moving as ""
                sat_color_band[td.css("::attr(bgcolor)").extract_first()] = ""

        tds = response.css('td[valign="top"]')[2:4]
        all_tr = []
        for td in tds:
            all_tr.extend(td.css("tr"))
        sat_orbit_tmp = ''
        for tr in all_tr:
            cnt += 1
            sat = SatItem()
            tr_tds = tr.css('td')
            orbit = ''
            name = ''
            band = ''
            time = ''
            url = ''
            if len(tr_tds) == 5:
                orbit1 = tr_tds[1].css('font::text').extract()
                orbit2 = tr_tds[1].css('a::text').extract_first()
                orbit = orbit2 + orbit1[0] + orbit1[1]  # ''.join(orbit1)#使用该函数有问题
                sat_orbit_tmp = orbit
                # print(orbit + " : " + area)
                name = tr_tds[2].css("a::text").extract_first()
                url = tr_tds[2].css("a::attr(href)").extract_first()
                sat_bgcolor = tr_tds[2].css("::attr(bgcolor)").extract_first()
                band = sat_color_band[sat_bgcolor]
                time = tr_tds[4].css("font::text").extract_first()
                if(time == None):
                    time = ''
            elif len(tr_tds) == 4:
                orbit = sat_orbit_tmp
                name = tr_tds[1].css("a::text").extract_first()
                url = tr_tds[1].css("a::attr(href)").extract_first()
                sat_bgcolor = tr_tds[1].css("::attr(bgcolor)").extract_first()
                band = sat_color_band[sat_bgcolor]
                time = tr_tds[3].css("font::text").extract_first()
                if time == None:
                    time = ''
            sat['area'] = area
            sat['orbit'] = orbit
            sat['name'] = name
            sat['band'] = band
            sat['time'] = time
            sat['url'] = url
            # print(sat)
            yield sat
            yield response.follow(url, callback=self.chanlist_parse_from_sat, meta={"sat_orbit": orbit, "sat_name": name, "sat_area": area, "sat_band": band, "sat_link": url})


    def chanlist_parse_from_sat(self, response):
        if 'sat_orbit' in response.meta.keys():
            sat_orbit = response.meta['sat_orbit']
        else:
            # sat_obit = ''
            return
        if 'sat_name' in response.meta.keys():
            sat_name = response.meta['sat_name']
        else:
            # sat_name = ''
            return
        if 'sat_band' in response.meta.keys():
            sat_band = response.meta['sat_band']
        else:
            sat_band = ''
        if 'sat_area' in response.meta.keys():
            sat_area = response.meta['sat_area']
        else:
            sat_area = ''
        if 'sat_link' in response.meta.keys():
            sat_link = response.meta['sat_link']
        else:
            sat_link = ''

        color_type = {}
        mycolor_type = {}
        color_tds = response.css('table[align="center"] td[bgcolor]')
        for td in color_tds:
            color_type[td.css('::attr(bgcolor)').extract_first()] = td.css("font::text").extract_first().strip()
        for (key, type) in color_type.items():
            if type == "SD/clear":
                mycolor_type[key] = 'FAT SD'
            if type == "SD/encrypted":
                mycolor_type[key] = 'Encrypted SD'
            if type == "HD/clear":
                mycolor_type[key] = 'FAT HD'
            if type == "HD/encrypted":
                mycolor_type[key] = 'Encrypted HD'
            if type == "internet/interactive":
                mycolor_type[key] = 'Interactive'
            if type == "feeds":
                mycolor_type[key] = 'Feeds'
            if type == "analog/clear":
                mycolor_type[key] = 'FAT Analog'
            if type == "analog/encrypted":
                mycolor_type[key] = 'Encrypted Analog'
        alltab = response.css('table[width="720"]')
        chan_tabs = []

        tp_freq = ''
        tp_pol = ''
        tp_mode = []
        tp_sym = ''
        tp_fec = ''
        tp_time = ''
        tp_desc = ''
        tp_footprints_text = ''
        for tab in alltab:
            if len(tab.css('td[colspan="10"][align="center"]')) > 0:
                chan_tabs.append(tab)
        for tab in chan_tabs:
            chan_trs = tab.css('tr')
            for (i,tr) in enumerate(chan_trs):
                # print("AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA" + str(i))
                name = ""
                chantype = ""
                vpid = ""
                group = ""
                apid = ""
                encryption = []
                sid = ""
                alltd = tr.css('td')
                if len(alltd) == 9 and i>1:
                    # print("pppppppppppppppppp" + str(i))
                    tp_freq = alltd[1].css("b::text").extract_first()
                    if tp_freq:
                        tp_freq.split(" ")[0].strip()
                    else:
                        tp_freq =''
                    # print(tp_freq)
                    tp_pol = alltd[1].css("b::text").extract_first()
                    if tp_pol:
                        tp_pol = tp_pol.strip()
                        # print(tp_pol)
                        if len(tp_pol.split(" ")) == 2:
                            tp_pol = tp_pol.split(" ")[1].strip()
                        else:
                            tp_pol = ''
                    else:
                        tp_pol = ''
                    # print(tp_pol)
                    modes = alltd[5].css('font::text').extract()
                    # print(modes)
                    tp_mode = list(map(lambda x: x.strip(), modes))
                    #print(tp_mode)
                    tp_sym = alltd[6].css('font::text').extract_first()
                    if tp_sym:
                        tp_sym = tp_sym.strip().split("-")[0]
                    else:
                        tp_sym = ''
                    # print(tp_sym)
                    tp_fec = alltd[6].css('font::text').extract_first()
                    if tp_fec:
                        tp_fec = tp_fec.strip().split("-")[1]
                    else:
                        tp_fec = ''
                    # print(tp_fec)
                    if tp_fec == "?" or tp_fec == ".":
                        tp_fec = ""
                    tp_time = alltd[8].css('font::text').extract()  # exception instance : Marcel 2 , Albert I ... , but time is not key property , don't worry
                    if len(tp_time) == 2:
                        tp_time = tp_time[1].strip()
                    else:
                        tp_time = ''
                    # print(tp_time)
                    tp_desc = alltd[3].css('a::text').extract_first() or alltd[3].css('font::text').extract_first()
                    if tp_desc:
                        tp_desc = tp_desc.strip()
                    else:
                        tp_desc = ''
                    # print(tp_desc)
                    tp = TpItem()
                    tp['sat_orbit'] = sat_orbit
                    tp['sat_link'] = sat_link
                    tp['freq'] = tp_freq
                    tp['pol'] = tp_pol
                    tp['sym'] = tp_sym
                    tp['fec'] = tp_fec
                    tp['mode'] = tp_mode
                    tp['desc'] = tp_desc
                    tp['time'] = tp_time
                    tp['footprints_text'] = tp_footprints_text
                    # print(tp)
                    yield tp
                else:
                    if len(alltd) == 7 and len(alltd[0].css("b")) == 0:
                        name = alltd[1].css("a::text").extract_first() or alltd[1].css("b::text").extract_first() or ""
                        # print(name)
                        type_color = alltd[1].css("::attr(bgcolor)").extract_first()
                        chantype = mycolor_type[type_color]
                        # print(chantype)
                        encryption = list(filter(lambda x: x.strip('\xa0').strip(), alltd[3].css('font::text').extract()))
                        # print(encryption)
                        sid = alltd[4].css("font::text").extract_first()
                        if sid:
                            sid = sid.strip()
                        else:
                            sid = ''
                        vpid = ''
                        apid = alltd[5].css("font::text").extract_first()
                        if apid:
                            apid = apid.strip()
                        else:
                            apid = ''
                        # print(apid)
                    elif len(alltd) == 7 and len(alltd[0].css("b")) > 0:
                        name = alltd[0].css("a::text").extract_first() or alltd[0].css("b::text").extract_first() or ""
                        # print(name)
                        type_color = alltd[0].css("::attr(bgcolor)").extract_first()
                        chantype = mycolor_type[type_color]
                        # print(chantype)
                        sid = alltd[3].css("font::text").extract_first().strip()
                        # print(sid)
                        vpid = alltd[4].css("font::text").extract_first().strip()
                        # print(vpid)
                        encryption = list(filter(lambda x: x.strip(), alltd[2].css('font::text').extract()))
                        # print(encryption)
                        apid = alltd[5].css("font::text").extract_first()
                        if apid:
                            apid = apid.strip()
                        else:
                            apid = ''
                        # print(apid)
                    elif len(alltd) == 6:
                        name = alltd[0].css("a::text").extract_first() or alltd[0].css("b::text").extract_first() or ""
                        # print(name)
                        encryption = list(filter(lambda x: x.strip(), alltd[2].css('font::text').extract()))
                        # print(encryption)
                        sid = alltd[3].css("font::text").extract_first()
                        if sid:
                            sid = sid.strip()
                        else:
                            sid = ''
                        # print(sid)
                        vpid = ''
                        apid = alltd[4].css("font::text").extract_first()
                        if apid:
                            apid = apid.strip()
                        else:
                            apid = ''
                        # print(apid)
                    else:
                        continue
                    if not name:
                        continue
                    chan = ChanItem()
                    chan['sat_orbit'] = sat_orbit
                    chan['sat_name'] = sat_name
                    chan['sat_band'] = sat_band
                    chan['sat_area'] = sat_area
                    chan['sat_link'] = sat_link
                    chan['tp_freq'] = tp_freq
                    chan['tp_pol'] = tp_pol
                    chan['tp_fec'] = tp_fec
                    chan['tp_sym'] = tp_sym
                    chan['tp_mode'] = tp_mode
                    chan['tp_footprints_text'] = tp_footprints_text
                    chan['name'] = name
                    chan['vpid'] = vpid
                    chan['apid'] = apid
                    chan['sid'] = sid
                    chan['chantype'] = chantype
                    chan['encryption'] = encryption
                    print(name, vpid, sid, apid, sat_orbit, sat_link)
                    yield chan

    def pkglist_parse(self, response):
        area = response.meta['area']
        cnt = 0
        pkg_color_band = {}
        color_tds = response.css('table[align="center"]')[1].css('td[bgcolor]')
        for td in color_tds:
            band_type = td.css("font::text").extract_first().strip()
            if band_type == "C band":
                pkg_color_band[td.css("::attr(bgcolor)").extract_first()] = "C"
            elif band_type == "C & Ku band":
                pkg_color_band[td.css("::attr(bgcolor)").extract_first()] = "C/Ku"
            elif band_type == "Ku band":
                pkg_color_band[td.css("::attr(bgcolor)").extract_first()] = "Ku"
            else:
                pkg_color_band[td.css("::attr(bgcolor)").extract_first()] = ""
        td = response.css('td[valign="top"]')[2]
        all_tr = []
        all_tr = td.css("tr")
        #for td in tds:
        #    all_tr.extend(td.css("tr"))
        for tr in all_tr:
            cnt += 1
            tr_tds = tr.css('td')
            sat_orbit = ''
            sat_name = ''
            sat_url = ''
            time = ''
            name = ''
            url = ''
            band = ''
            #if(len(tr_tds)<2):
            #    logging.info(response.meta['req_url']+ " : " + tr.extract())
            orbit1 = tr_tds[1].css('a::text').extract_first()
            #print(orbit1)

            orbit2 = tr_tds[1].css('font::text').extract()
            sat_orbit = orbit1 + orbit2[0] + orbit2[1]  # ''.join(orbit1)#使用该函数有问题
            sat_name = tr_tds[2].css("a::text").extract_first()
            sat_url = tr_tds[2].css("a::attr(href)").extract_first()
            url = tr_tds[3].css("a::attr(href)").extract_first()
            name = tr_tds[3].css("a::text").extract_first()
            time = tr_tds[7].css("font::text").extract_first()
            if time == None:
                time = ''
            pkg_bgcolor = tr_tds[3].css("::attr(bgcolor)").extract_first()
            band = pkg_color_band[pkg_bgcolor]
            pkg = PkgItem()
            pkg['name'] = name
            pkg['url'] = url
            pkg['sat_orbit'] = sat_orbit
            pkg['sat_name'] = sat_name
            pkg['time'] = time
            pkg['sat_url'] = sat_url
            pkg['band'] = band
            pkg['area'] = area
            #logging.info(name+ " : " +response.meta['req_url'])
            #print(pkg)
            yield pkg
            yield response.follow(url, callback=self.chanlist_parse, meta={"sat_orbit": sat_orbit, "sat_name": sat_name, "sat_link":sat_url, "pkg_link":url, "pkg_name":name, "req_url": url, "area": area, "pkg_band": band})

    def chanlist_parse(self, response):
        cnt = 0
        if 'sat_orbit' in response.meta.keys():
            sat_orbit = response.meta['sat_orbit']
        else:
            sat_obit = ''
        if ('sat_name' in response.meta.keys()):
            sat_name = response.meta['sat_name']
        else:
            sat_name = ''
        if 'sat_band' in response.meta.keys():
            sat_band = response.meta['sat_band']
        else:
            sat_band = ''
        if 'sat_area' in response.meta.keys():
            sat_area = response.meta['sat_area']
        else:
            sat_area = ''
        if 'sat_link' in response.meta.keys():
            sat_link = response.meta['sat_link']
        else:
            sat_link = ''
        if 'pkg_link' in response.meta.keys():
            pkg_link = response.meta['pkg_link']
        else:
            pkg_link = ''
        if 'pkg_name' in response.meta.keys():
            pkg_name = response.meta['pkg_name']
        else:
            pkg_name = ''
        if 'area' in response.meta.keys():
            area = response.meta['area']
        else:
            area = ''
        if 'pkg_band' in response.meta.keys():
            pkg_band = response.meta['pkg_band']
        else:
            pkg_band = ''
        tp_freq = ''
        tp_pol = ''
        tp_fec = ''
        tp_sym = ''
        tp_mode = []
        tp_footprints_text = ''
        name = ''
        vpid = ''
        apid = ''
        sid = ''
        desc = ''
        logo_url = ''
        chno = ''
        system = ''
        chantype = ''
        encryption = []

        color_type = {}
        mycolor_type = {}
        color_tds = response.css('table[align="center"] td[bgcolor]')
        for td in color_tds:
            color_type[td.css('::attr(bgcolor)').extract_first()] = td.css("font::text").extract_first().strip()
        for (key,type) in color_type.items():
            if type=="SD/clear":
                mycolor_type[key] = 'FAT SD'
            if type=="SD/encrypted":
                mycolor_type[key] = 'Encrypted SD'
            if type=="HD/clear":
                mycolor_type[key] = 'FAT HD/UHD'
            if type=="HD/encrypted":
                mycolor_type[key] = 'Encrypted HD/UHD'
            if type=="interactive":
                mycolor_type[key] = 'Interactive'
        tbs = response.css('table[width="720"]')[6:-1] #one package may have more than one table
        trs = []
        if len(tbs)<1:
            #print("no hitttttt",response.meta('req_url'))
            return
        for tb in tbs:
            if(tb!=None):
                trs.extend(tb.css("tr")) #trs = trs.extend(tb.css("tr")) # wrong usage

        for tr in trs:
            tds = tr.css('td')
            tds_len_tmp = len(tds)
            if tds_len_tmp==1: # remove head tr and foot tr
                continue
            elif tds_len_tmp==10: #eg:https://www.lyngsat.com/packages/Vini.html
                if tds[2].css("font::text").extract_first()=="Channel Name": # remove table header
                    continue

                freqs = tds[0].css("b::text").extract_first().strip()
                if freqs == None:
                    freq = ""
                else:
                    tp_freq = freqs.split(" ")[0]
                    tp_pol = freqs.split(" ")[1]

                tps = tds[0].css("font::text").extract()
                tp_name = tps[0]
                tp_area = tps[1]
                tp_footprints_text = str(tp_name) + "|" + str(tp_area)

                try:
                    tp_fec = tds[0].css("font::text").extract()[-1].split("FEC")[1].strip(" ")
                except IndexError:
                    logging.info("IndexError happened : " + pkg_link + freqs)
                if(tp_fec == None):
                    tp_fec = ""

                for it in tps:
                    if "SR" in str(it):
                        tp_sym = str(it).split("SR")[1].strip(" ")
                        break

                modes = tds[0].css("font::text").extract()[2:-2]
                #logging.info(str(modes))
                if(len(modes)>0):
                    #mode = ",".join(modes)
                    tp_mode = modes
                else:
                    tp_mode = []

                tp = TpItem()
                tp['sat_orbit'] = sat_orbit
                tp['sat_link'] = sat_link
                tp['pkg_name'] = pkg_name
                tp['pkg_link'] = pkg_link
                tp['freq'] = tp_freq
                tp['pol'] = tp_pol
                tp['sym'] = tp_sym
                tp['fec'] = tp_fec
                tp['mode'] = tp_mode
                tp['footprints_text'] = tp_footprints_text
                #print(tp)
                yield tp

                logo_url = tds[1].css('img::attr("src")').extract_first()
                if (logo_url):
                    logo_url = response.urljoin(logo_url)
                else:
                    logo_url = ""
                name = tds[2].css('a::text').extract_first()
                if name==None:
                    name = ""
                encpts = tds[4].css('font::text').extract()
                update_infos = tds[9].css('font::text').extract()
                if len(encpts) > 0 and len(update_infos) > 0:
                    desc = ','.join(encpts) + '|' + ','.join(update_infos)
                elif len(encpts) > 0:
                    desc = ','.join(encpts)
                elif len(update_infos) > 0:
                    desc = ','.join(update_infos)
                chno = tds[5].css('font::text').extract_first()
                if chno==None:
                    chno = ""
                else:
                    chno = chno.strip(" ")
                sid = tds[6].css('font::text').extract_first()
                if(sid==None):
                    sid =""
                else:
                    sid = sid.strip("\xa0")
                vpid = tds[7].css('font::text').extract_first()
                if (vpid == None):
                    vpid == ""
                else:
                    vpid = vpid.strip("\xa0")
                apid = "".join(tds[8].css('font::text').extract_first())
                if apid == None:
                    apid == None
                else:
                    apid = apid.strip("\xa0")
                    apid = apid.strip("\xa0E")
                    apid = apid.strip("\xa0F")
                    apid = " ".join(apid.split("\xa0"))
                cnt += 1
                bgcolor = tds[2].css("::attr(bgcolor)").extract_first()
                if bgcolor in mycolor_type.keys(): #no color_type , remove
                    chantype = mycolor_type[bgcolor]
                else:
                    continue
                encryption = tds[4].css('font::text').extract()
                encryption = list(map(lambda x:x.strip(), encryption))
            elif tds_len_tmp == 9: #have same orbit with first tr
                logo_url = tds[0].css('img::attr("src")').extract_first()
                if(logo_url):
                    logo_url = response.urljoin(logo_url)
                else:
                    logo_url = ""
                name = tds[1].css('a::text').extract_first()
                if (name == None):
                    name = ""
                encpts = tds[3].css('font::text').extract()
                update_infos = tds[8].css('font::text').extract()
                if len(encpts)>0 and len(update_infos)>0:
                    desc = ','.join(encpts) + '|' + ','.join(update_infos)
                elif len(encpts) > 0:
                    desc = ','.join(encpts)
                elif len(update_infos)>0:
                    desc = ','.join(update_infos)
                chno = tds[4].css('font::text').extract_first()
                if chno == None:
                    chno = ""
                else:
                    chno = chno.strip(" ")
                sid = tds[5].css('font::text').extract_first()
                if (sid == None):
                    sid = ""
                else:
                    sid = sid.strip("\xa0")
                vpid = tds[6].css('font::text').extract_first()
                if (vpid==None):
                    vpid == ""
                else:
                    vpid = vpid.strip("\xa0")
                apid = "".join(tds[7].css('font::text').extract())
                if (apid == None):
                    apid == ""
                else:
                    apid = apid.strip("\xa0")
                    apid = apid.strip("\xa0E")
                    apid = apid.strip("\xa0F")
                    apid = " ".join(apid.split("\xa0"))
                cnt += 1
                bgcolor = tds[1].css("::attr(bgcolor)").extract_first()
                if bgcolor in mycolor_type.keys(): #no color_type , remove
                    chantype = mycolor_type[bgcolor]
                else:
                    continue
                encryption = tds[3].css('font::text').extract()
                encryption = list(map(lambda x: x.strip(), encryption))

            elif tds_len_tmp==8: #https://www.lyngsat.com/packages/DirecTV-USA-119W.html
                if(tds[0].css("font::text").extract_first() == "Ch"):
                    #logging.info("find table header !!!")
                    continue
                chno = tds[0].css('b::text').extract_first()
                if chno==None or chno=="":
                    chno = ""
                else:
                    chno = chno.strip(" ").strip("\xa0")
                freq = tds[1].css('font::text').extract_first()
                if freq == None or freq=="":
                    freq = ""
                else:
                    freq = freq.strip(" ").strip("\xa0")
                system = tds[2].css('nobr::text').extract_first()
                if system == None or system=="":
                    system = ""
                else:
                    system = system.strip(" ").strip("\xa0")
                logo_url = tds[3].css('img::attr("src")').extract_first()
                if logo_url:
                    logo_url = response.urljoin(logo_url)
                else:
                    logo_url = ""
                name1 = tds[4].css('a::text').extract_first()
                name2 = "".join(tds[4].css('font::text').extract())
                name = str(name1) + name2
                if name == None:
                    name = ""
                encpts = tds[6].css('font::text').extract()
                update_infos = tds[7].css('font::text').extract()
                if len(encpts) > 0 and len(update_infos) > 0:
                    desc = ','.join(encpts) + '|' + ','.join(update_infos)
                elif len(encpts) > 0:
                    desc = ','.join(encpts)
                elif len(update_infos) > 0:
                    desc = ','.join(update_infos)
                #logging.info(chno + "," + freq + "," + system + "," + logo_url + "," + name + "," + desc + "," + response.meta['req_url'])
                cnt += 1
                bgcolor = tds[4].css("::attr(bgcolor)").extract_first()
                if bgcolor in mycolor_type.keys(): #no color_type , remove
                    chantype = mycolor_type[bgcolor]
                else:
                    continue
                encryption = tds[6].css('font::text').extract()
                encryption = list(map(lambda x: x.strip(), encryption))
            if vpid==None:  #fix empty as None
                vpid = ""
            if name=="":
                continue    #remove test channel , eg: https: // www.lyngsat.com / packages / Claro - Chile.html
            chan = ChanItem()
            chan['sat_orbit'] = sat_orbit
            chan['sat_name'] = sat_name
            chan['sat_band'] = sat_band
            chan['sat_area'] = sat_area
            chan['sat_link'] = sat_link
            chan['pkg_link'] = pkg_link
            chan['pkg_name'] = pkg_name
            chan['pkg_area'] = area
            chan['pkg_band'] = pkg_band
            chan['tp_freq'] = tp_freq
            chan['tp_pol'] = tp_pol
            chan['tp_fec'] = tp_fec
            chan['tp_sym'] = tp_sym
            chan['tp_mode'] = tp_mode
            chan['tp_footprints_text'] = tp_footprints_text
            chan['name'] = name
            chan['vpid'] = vpid
            chan['apid'] = apid
            chan['sid'] = sid
            chan['desc'] = desc
            chan['logo_url'] = logo_url
            chan['chno'] = chno
            chan['system'] = system
            chan['chantype'] = chantype
            chan['encryption'] = encryption
            print(name, vpid, sid, apid, response.meta['req_url'])
            #if (cnt > 10):
            #    break
            yield chan









