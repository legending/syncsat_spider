import scrapy
from scrapy.selector import Selector
from sat_spider.items import SatItem, TpItem, PkgItem, ChanItem, FootPrintsItem

class FlySatSpider(scrapy.Spider):
    name = "flysat"

    def start_requests(self):
        satlist="http://www.flysat.com/satlist.php"
        packagelist="http://www.flysat.com/package.php"
        pkgtest="http://www.flysat.com/canalfrance.php"
        sattest="http://www.flysat.com/hispasat.php"
        chanlist_test = "http://www.flysat.com/astra19.php"

        yield scrapy.Request(url=satlist, callback=self.satlist_parse)
        yield scrapy.Request(url=packagelist, callback=self.packagelist_parse)
        #yield scrapy.Request(url=pkgtest, callback=self.chanlist_parse)
        #yield scrapy.Request(url=sattest, callback=self.chanlist_parse)
        #yield scrapy.Request(url=chanlist_test, callback=self.chanlist_parse)
    
    def chanlist_tp_parse(self, tr, response):
        if 'father' in response.meta.keys():
            father = response.meta['father']
        if 'sat_orbit' in response.meta.keys() :
            sat_orbit = response.meta['sat_orbit']
        else:
            sat_orbit = ''
        if 'sat_name' in response.meta.keys() :
            sat_name = response.meta['sat_name']
        else:
            sat_name = ''
        if 'pkg_name' in response.meta.keys() :
            pkg_name = response.meta['pkg_name']
        else:
            pkg_name = ''

        tp = TpItem()
        tp['sat_orbit'] = sat_orbit
        # tp['sat_name'] = sat_name
        if father == 'sat':
            tp['sat_link'] = response.request.url
        elif father == 'pkg':
            tp['pkg_link'] = response.request.url

        alltd = tr.css('td')
        timesel = alltd[0].css("font::text")
        #print(len(timesel))
        tp['time'] = timesel[-1].extract()
        #alltd[0].css("font::text")[2].extract()
        tmp = alltd[1].css("b::text").extract_first()
        tp['freq'] = tmp.split(" ")[0]
        tp['pol'] = tmp.split(" ")[1]
        #tp['pol'] = tmp[1]
        mode = []
        tmpall = alltd[1].css("font::text")
        for tmp in tmpall:
            tm = tmp.extract()
            mode.append(tm)
        tp['mode'] = mode
        '''
        tmp = alltd[1].css("font::text").extract_first().split('/')
        tp['dvbmode'] = tmp[0]     
        if(tmp[0].endswith('2')):
            if(len(tmp) > 1):
                tp['pskmode'] = tmp[1]
                tmp2 = alltd[1].css("font::text")
                if(len(tmp2) > 1):
                    tp['videomode'] = tmp2[1].extract()
                else:
                    tp['videomode'] = tmp[1]
            else:
                tmp2 = alltd[1].css("font::text")
                if(len(tmp2) > 1):
                    tp['pskmode'] = tmp2[1].extract()
        '''

        #print(alltd[2].css("font::text"))
        tmptxt = alltd[2].css("font::text").extract_first()
        if tmptxt is not None and len(tmptxt) > 1:
            tmp = tmptxt.split(' ')
            tp['sym'] = tmp[0]
            if len(tmp) > 1:
                tp['fec'] = tmp[1]
        else:
            tp['sym'] = ""
            tp['fec'] = ""
        align = alltd[3].css("td[align]")
        if align:
            #attention !!!!
            #here only for package in sat list without package page link
            tp['pkg_name'] = alltd[3].css("b::text").extract_first()
            if tp['pkg_name'] is not None:
                #revise add : strip()
                tp['pkg_name'] = tp['pkg_name'].strip('\xa0').strip()
                tp['pkg_web'] = alltd[4].css("a::attr(href)").extract_first()
        else:
            tp['pkg_name'] = ''
        if pkg_name:
            tp['pkg_name'] = pkg_name #confused: where pkg_name is correct ( there may be override)

        colspan = alltd[5].css("td::attr(colspan)").extract_first()
        desc_idx = 7
        fps_idx = 8
        if(colspan is not None and colspan == "3"):
            desc_idx = None
            fps_idx = 6
        elif(colspan is not None and  colspan == "2"):
            desc_idx = 6
            fps_idx = 7
        
        if desc_idx is not None:
            descs = alltd[desc_idx].css("font::text")
            desc = []
            for tmp in descs:
                desc.append(tmp.extract())
                #desc = desc + tmp.extract() + ','
            tp['desc'] = desc
        else:
            tp['desc'] = ""
        #error: desc index error , referring to dbdata and http://www.flysat.com/expressat2.php
        footprints = ''
        #print('tp:' + tp['freq'] + ": " + str(len(alltd)))
        fps = alltd[fps_idx].css("a::text")
        for tmp in fps:
            footprints = footprints + tmp.extract() + ','
        tp['footprints_text'] = footprints
        fpsurl = alltd[fps_idx].css("a::attr(href)").extract_first()
        #patch
        if fpsurl is not None and fpsurl.startswith('htp'):
            fpsurl = 'htt' + fpsurl[2:]
        elif fpsurl is not None and fpsurl.startswith('ttp'):
            fpsurl = 'htt' + fpsurl[2:]
        return tp, fpsurl

    def chanlist_parse(self, response):
        if'father' in response.meta.keys():
            father = response.meta['father']
        if 'sat_orbit' in response.meta.keys():
            sat_orbit = response.meta['sat_orbit']
        else:
            sat_orbit = ''
        if 'sat_name' in response.meta.keys():
            sat_name = response.meta['sat_name']
        else:
            sat_name = ''
        if 'sat_band' in response.meta.keys():
            sat_band = response.meta['sat_band']
        else:
            sat_band = ''
        if 'sat_area' in response.meta.keys():
            sat_area = response.meta['sat_area']
        else:
            sat_area = ''
        if 'sat_link' in response.meta.keys():
            sat_link = response.meta['sat_link']
        else:
            sat_link = ''

        if 'pkg_name' in response.meta.keys():
            pkg_name = response.meta['pkg_name']
        else:
            pkg_name = ''
        if 'pkg_band' in response.meta.keys():
            pkg_band = response.meta['pkg_band']
        else:
            pkg_band = ''
        if 'pkg_area' in response.meta.keys():
            pkg_area = response.meta['pkg_area']
        else:
            pkg_area = ''
        if 'pkg_link' in response.meta.keys():
            pkg_link = response.meta['pkg_link']
        else:
            pkg_link = ''

        color_code_table = response.css("table[width='950'][bordercolor='#000099'][cellpadding='2'][cellspacing='2']")
        color_code_tds = color_code_table.css('td')
        chan_type_color = {}
        mycolor_type = {}
        for td in color_code_tds:
            color = td.css('td::attr(bgcolor)')[0].extract()
            chan_type = td.css('font::text')[0].extract()
            chan_type_color[color] = chan_type
        #print(chan_type_color)
        for (color,type) in chan_type_color.items():
            if type=="FTA MPEG-2":
                mycolor_type[color] = "FAT SD"
            if type=="FTA MPEG-4":
                mycolor_type[color] = "FAT HD"
            if type=="FTA HEVC" or type=="FTA HD/UHD":
                mycolor_type[color] = "FTA HD/UHD"
            if type=="Encrypted SD":
                mycolor_type[color] = "Encrypted SD"
            if type=="Encrypted HD/UHD":
                mycolor_type[color] = "Encrypted HD/UHD"
            if type=="Data":
                mycolor_type[color] = "Data"
            if type=="Feed":
                mycolor_type[color] = "Feed"
            if type=="Temporary FTA":
                mycolor_type[color] = "Temporary FTA"
        table = response.css("table[bordercolor='#3366cc']")
        trs = table.css('tr')
        curfreq = ""
        curpol = ""
        curgroup = ""
        curpkgname = ""
        curpkgweb = ""
        cursym = ""
        curfec = ""
        curmode = []
        cur_footprints_text = ''
        plain_pkg_list = []
        encryption_head = []
        for tr in trs:
            bgcolor = tr.css('tr::attr(bgcolor)').extract_first()
            if bgcolor == '#79bcff':
                curgroup = ""
                curpkgname = ""
                curpkgweb = ""

                alltd = tr.css('td')
                encryption_head = alltd[7].css("font::text").extract()
                encryption_head = list(map(lambda x: x.strip('\xa0').strip(),encryption_head))
                encryption_head = list(filter(lambda x: x,encryption_head))
                tp,fpsurl = self.chanlist_tp_parse(tr, response)
                cursym = tp['sym']
                curmode = tp['mode']
                curfec = tp['fec']
                curfreq = tp['freq']
                curpol = tp['pol']
                cur_footprints_text = tp['footprints_text']
                if 'pkg_name' in tp.keys() and tp['pkg_name'] is not None and tp['pkg_name'] != "":
                    curpkgname = tp['pkg_name']

                    print("pkg name : " + curpkgname)# chanlist_tp_parse return no link package , just plain pkg , so there is no need to check
                    pkg = PkgItem()
                    pkg['name'] = tp['pkg_name'] or pkg_name
                    pkg['sat_orbit'] = sat_orbit
                    pkg['sat_name'] = sat_name
                    pkg['time'] = ''
                    pkg['url'] = pkg_link
                    pkg['band'] = pkg_band or sat_band
                    yield pkg
                if 'pkg_web' in tp.keys() and tp['pkg_web'] is not None:
                    curpkgweb = tp['pkg_web']

                footprints = FootPrintsItem()
                footprints['sat_orbit'] = sat_orbit
                footprints['freq'] = curfreq
                footprints['pol'] = curpol
                #confused: why #79bcff to get tp , other bgcolor items also have tp -> got it : every tr mostly is #79bcff
                #confused: where to remove repeated items -> in pipelines
                #yield response.follow(fpsurl, callback=self.footprints_parse, meta={'footprints': footprints})
                yield tp
            
            elif bgcolor == '#ffffff':
                curgroup = tr.css('b::text').extract_first()

            elif bgcolor in mycolor_type.keys():
                chan = ChanItem()
                if curgroup is not None and len(curgroup) > 0:
                    chan['group'] = curgroup
                chan['chantype'] = mycolor_type[bgcolor]
                chan['tp_freq'] = curfreq
                chan['tp_pol'] = curpol
                if curpkgname is not None:
                    chan['pkg_name'] = curpkgname
                if curpkgweb is not None:
                    chan['pkg_web'] = curpkgweb
                chan['sat_orbit'] = sat_orbit
                chan['sat_name'] = sat_name
                alltd = tr.css('td')
                #print(alltd[0].css("font::text"))
                #print(alltd[0].css("b::text"))
                chan['name'] = alltd[0].css("b::text").extract_first()
                if chan['name'] == 'R':
                    rtxts = alltd[0].css("font::text")
                    chan['name'] = rtxts[0].extract() + chan['name'] + rtxts[1].extract()
                if chan['name'] is None:
                    chan['name'] = alltd[0].css("i::text").extract_first()
                    if (chan['name'] is None):
                        chan['name'] = alltd[0].css("font::text").extract_first()
                    
                chan['web'] = alltd[1].css("a::attr(href)").extract_first()
                if len(alltd) == 3: # http://www.flysat.com/hispasat.php 12518 H
                    chan['encryption'] = encryption_head
                    colspan = alltd[2].css("td::attr(colspan)").extract_first()
                    if colspan is not None and int(colspan) == 3:
                        desc = []
                        desc.append(alltd[2].css("font::text").extract_first())
                        chan['desc'] = desc
                elif len(alltd) >= 4:
                    #print('chan:' + chan['freq'] + ": " + str(len(alltd)))
                    colspan = alltd[2].css("td::attr(colspan)").extract_first()
                    chan['vpid'] = alltd[2].css("font::text").extract_first()
                    if chan['vpid'] is not None:
                        chan['vpid'] = chan['vpid'].strip('\xa0')
                    if colspan is not None and int(colspan) == 2:
                        siddescs = alltd[3].css("font::text")
                    else:
                        chan['apid'] = alltd[3].css("font::text").extract_first()
                        if chan['apid'] is None:
                            chan['apid'] = alltd[3].css("b::text").extract_first()
                        if(chan['apid'] is not None):
                            chan['apid'] = chan['apid'].strip('\xa0')       
                        siddescs = alltd[4].css("font::text")
                    chan['sid'] = siddescs.extract_first()
                    if chan['sid'] is not None:
                        chan['sid'] = chan['sid'].strip('\xa0') 
                    desc = []
                    if len(siddescs) > 1:
                        for tmp in siddescs[1:]:
                            desc.append(tmp.extract())
                            #desc = desc + tmp.extract() + ','
                    chan['desc'] = desc
                    encode = list(filter(lambda txt: not txt.isdigit() and txt.strip(), siddescs.extract()))
                    encryption = encode + encryption_head
                    chan['encryption'] = list(filter(lambda x:len(x)>0, encryption))
                chan['sat_orbit'] = sat_orbit
                chan['sat_name'] = sat_name
                chan['sat_band'] = sat_band
                chan['sat_area'] = sat_area
                chan['sat_link'] = sat_link
                chan['pkg_name'] = pkg_name
                chan['pkg_area'] = pkg_area
                chan['pkg_band'] = pkg_band
                chan['pkg_link'] = pkg_link
                chan['tp_freq'] = curfreq
                chan['tp_pol'] = curpol
                chan['tp_fec'] = curfec
                chan['tp_sym'] = cursym
                chan['tp_mode'] = curmode
                chan['tp_footprints_text'] = cur_footprints_text
                yield chan

    def footprints_parse(self, response):
        footprints = response.meta['footprints']
        tds = response.css("td[bgcolor='#3CA4FF']")
        footprints['image_urls'] = []
        for td in tds:
            images = td.css("img::attr(src)")
            for image in images:
                img = image.extract()
                if(img is not None):
                    #print(img)
                    footprints['image_urls'].append(img)
        yield footprints


    def packagelist_parse(self, response):
        pkgs= response.css("tr[bgcolor='#40e0d0']")
        cnt = 0
        for pkg in pkgs:
            a = pkg.css('td')[0]
            sat_href = a.css("a::attr(href)").extract_first()
            if sat_href is None:
                sat_href = ''
            else:
                sat_href = sat_href.strip('\xa0')           
            sat_name = a.css("a::text").extract_first()
            if sat_name is None:
                sat_name = ''
            else:
                sat_name = sat_name.strip('\xa0')
            b = pkg.css('td')[1]
            href = b.css("a::attr(href)").extract_first()
            if href is None:
                href = ''
            else:
                href = href.strip('\xa0')
            name = b.css("a::text").extract_first()
            if name is None:
                name = ''
            else:
                name = name.strip('\xa0')
            c = pkg.css('td')[2]
            orbit = c.css("font[color='#000099']::text").extract_first()
            orbit2 = c.css("font[size='1']::text").extract_first()
            orbit3 = c.css("b::text").extract_first()
            if orbit is None:
                orbit = ''
            if(orbit2 is None):
                orbit2 = ''
            if(orbit3 is None):
                orbit3 = ''
            orbit = orbit.strip('\xa0')
            orbit2 = orbit2.strip('\xa0')
            orbit3 = orbit3.strip('\xa0')
            orbit = orbit + orbit2 + orbit3

            d = pkg.css('td')[3]
            band = d.css('font::text').extract_first()
            if band is None:
                band = ''
            band = band.strip('\xa0')
            e = pkg.css('td')[4]
            time = e.css('font::text').extract_first()
            if(time is None):
                time = ''
            time = time.strip('\xa0')

            item = PkgItem()
            item['name'] = name.strip('\xa0')
            item['url'] = response.urljoin(href)
            item['sat_name'] = sat_name.strip('\xa0')
            item['sat_orbit'] = orbit.strip('\xa0')
            item['band'] = band.strip('\xa0')
            item['time'] = time.strip('\xa0')
            item['sat_url'] = response.urljoin(sat_href)

            if len(item['name']) > 0 and len(item['sat_orbit']) > 0:
                cnt = cnt+1
                yield item
                yield response.follow(item['url'], callback=self.chanlist_parse, meta={'sat_orbit': item['sat_orbit'], 'sat_name': item['sat_name'], "sat_link": item['sat_url'], "pkg_name": item['name'], "pkg_band": item['band'], "pkg_link": item['url'], 'father': 'pkg'})

    def satlist_parse(self, response):
        area_trs = {}
        trs = response.css("table")[10].css('tr[align="center"]')[:6]
        for (i,tr) in enumerate(trs):
            if i%2 == 0:
                td_area = tr.css('td[bgcolor="#FFFF99"][colspan="2"]')[0]
                if td_area:
                    area = td_area.css("font::text").extract_first().strip()
                    cur_trs = trs[i+1].css("tr[bgcolor='#b9dcff']")
                    if area == "A S I A":
                        area_trs['Asia'] = cur_trs
                    if area == "EUROPE - MIDDLE EAST - AFRICA":
                        area_trs['Europe-MiddleEast-Africa'] = cur_trs
                    if area == "A T L A N T I C":
                        area_trs['Atlantic'] = cur_trs
        '''
        sat1 = response.css("tr[bgcolor='#ffffff']")
        sat2 = response.css("tr[bgcolor='#55ccff']")
        sat3 = response.css("tr[bgcolor='#c9c9c9']")
        sats.extend(sat1)
        sats.extend(sat2)
        sats.extend(sat3)
        '''

        cnt = 0
        for (area,sats) in area_trs.items():
            for sat in sats:
                a = sat.css('td')[0]
                href = a.css("a::attr(href)").extract_first()
                if(href is None):
                    href = ''
                else:
                    href = href.strip('\xa0')
                name = a.css("a::text").extract_first()

                b = sat.css('td')[1]
                orbit = b.css("font[color='#000099']::text").extract_first()
                orbit2 = b.css("font[size='1']::text").extract_first()
                orbit3 = b.css("b::text").extract_first()
                if orbit is None:
                    orbit = ''
                if orbit2 is None:
                    orbit2 = ''
                if orbit3 is None:
                    orbit3 = ''
                orbit = orbit.strip('\xa0')
                orbit2 = orbit2.strip('\xa0')
                orbit3 = orbit3.strip('\xa0')
                orbit = orbit + orbit2 + orbit3

                c = sat.css('td')[2]
                band = c.css('font::text').extract_first()
                if band is None:
                    band = ''
                band = band.strip('\xa0')
                d = sat.css('td')[3]
                time = d.css('font::text').extract_first()
                if time is None:
                    time = ''
                time = time.strip('\xa0')

                item = SatItem()
                item['name'] = name
                item['url'] = response.urljoin(href)
                item['orbit'] = orbit
                item['band'] = band
                item['time'] = time
                item['area'] = area

                if len(item['orbit'])>0 and len(item['name'])>0:
                    cnt += 1
                    yield item
                    #revise
                    yield response.follow(item['url'], callback=self.chanlist_parse, meta={'sat_orbit': orbit,'sat_name': name, 'sat_band': band, "sat_area": area, "sat_link": item['url'], 'father': 'sat'})
