import pymongo
from pymongo import MongoClient

client=MongoClient('127.0.0.1',27017)
db=client.syncsat

sat_list = db.sats.find()
pkg_list = db.packages.find()
tp_list = db.tps.find()
chan_list = db.channels.find()

sat_list = list(sat_list[:])
pkg_list = list(pkg_list[:])
tp_list = list(tp_list[:])
chan_list = list(chan_list[:])

for sat in sat_list:
    sat_tp_chans = {}
    sat_tp_chans['sat'] = sat
    sat_tp_chans['tps'] = []
    tps = []


    for tp in tp_list:
        if tp['sat'] == sat['_id']:
            tp_item = tp
            tp_item['chans'] = []
            chans = []

            for chan in chan_list:
                if chan['tp'] == tp['_id']:
                    print("sat bingo : " + str(chan['_id']))
                    chans.append(chan)

            tp_item['chans'] = chans

            tps.append(tp_item)

    sat_tp_chans['tps'] = tps

    db.satchans.insert(sat_tp_chans)
    db.satchans.save(sat_tp_chans)

for pkg in pkg_list:
    pkg_tp_chans = {}
    pkg_tp_chans['pkg'] = pkg
    pkg_tp_chans['tps'] = []
    tps = []


    for tp in tp_list:
        if 'pkgs' in tp.keys():
            for p in tp['pkgs']:
                if p == pkg['_id']:
                    tp_item = tp
                    tp_item['chans'] = []

                    chans = []
                    for chan in chan_list:
                        if chan['tp'] == tp['_id']:
                            chans.append(chan)
                            print("pkg bingo : " + str(chan['_id']))
                    tp_item['chans'] = chans

                    tps.append(tp_item)

    pkg_tp_chans['tps'] = tps

    db.pkgchans.insert(pkg_tp_chans)
    db.pkgchans.save(pkg_tp_chans)
