# -*- coding: utf-8 -*-
import requests
from pymongo import MongoClient
import os
from PIL import Image
from io import BytesIO
import threadpool
import time

img_save_path = "/home/robin/repository/syncsat_spider/footprints_spider/images"

def download(beam):
    for sat in beam['data']['beams']:
        img_name = sat['name'] + '_tn' + '.gif'
        url = 'http://satbeams.com/images/foot_thumbs/' + img_name
        img_path = img_save_path + '/' + img_name

        headers = {
            'User-Agent': 'User-Agent:Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36',
            'Connection': 'close'
        }
        if (len(img_name) < 1):
            return
        response = requests.get(url, headers=headers)
        if (response.status_code == 200):
            if not os.path.exists(img_path):
                image = Image.open(BytesIO(response.content))
                image.save(img_path)
                print("saved " + img_name)
            else:
                print(img_name + " existed")
        else:
            return

if( __name__ == "__main__"):
    client = MongoClient('127.0.0.1', 27017)
    db = client.syncsat
    beam_list = list(db.beams.find({})[:])
    pool = threadpool.ThreadPool(50)
    start_time = time.time()
    reqs = threadpool.makeRequests(download, beam_list)
    for req in reqs:
        pool.putRequest(req)
    pool.wait()
    end_time = time.time()
    print('cost %d seconds' % (end_time - start_time))