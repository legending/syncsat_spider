# -*- coding: utf-8 -*-
import scrapy
import json
from footprints_spider.items import Footprints,Beam,BeamImg

class Spider(scrapy.Spider):
    name = "satbeams"
    allowed_domains = ["satbeams.com"]
    #download_delay = 2
    footprints_url = 'http://satbeams.com/footprints'
    beam_ajax_url = "http://satbeams.com/!ajax/footprint?action=get"
    print(footprints_url)

    def start_requests(self):
        yield scrapy.Request(url=self.footprints_url,callback=self.parse_footprint)

    def parse_footprint(self, response):
        #print(response.text)
        initdata_str = response.css('script')[17].extract().split("var data = ")[1].split(";")[0]
        #print(initdata_str)
        init_data = json.loads(initdata_str)
        #print(init_data)
        footprint = Footprints()
        footprint['page'] = init_data['page']
        yield footprint

        positions = init_data['page']['positions']
        for key in positions:
            cur_ajax_url = self.beam_ajax_url + "&position=" + str(key)
            yield response.follow(cur_ajax_url, callback=self.parse_beam, meta={"position": key})

    def parse_beam(self, response):
        data = json.loads(response.text.split("Server return: ")[1].split("<hr")[0].replace("\n", ""))
        position = response.meta['position']
        beam = Beam()
        beam['position'] = position
        beam['data'] = data['data']
        log_str = "position : " + str(beam['position']) + " , " + "beam num : " + str(len(data['data']['beams']))
        print(log_str)
        yield beam

        sat_list = data['data']['beams']
        beamImg = BeamImg()
        beamImg['position'] = position
        beamImg['image_urls'] = []
        #beamImg['images'] = []
        image_urls = []
        #beamImg['images'] = []
        for (i,item) in enumerate(sat_list):
            img_name = item['name'] + '_tn' + '.gif'
            img_url = 'http://satbeams.com/images/foot_thumbs/' + img_name
            image_urls.append(img_url)
        beamImg['image_urls'] = image_urls
        beamImg['images'] = []
        yield beamImg


