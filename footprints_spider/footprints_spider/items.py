# -*- coding: utf-8 -*-

# Define here the models for your scraped items
#
# See documentation in:
# http://doc.scrapy.org/en/latest/topics/items.html

import scrapy

class Footprints(scrapy.Item):
    page = scrapy.Field()

class Beam(scrapy.Item):
    # define the fields for your item here like:
    position = scrapy.Field()
    data = scrapy.Field()
    '''
    {
        "position": 5,
        "data": {
            "beams": [{
                "id": "5679",
                "name": "5E-Ku-Africa",
                "band": "Ku",
                "fullname": "Ku-band Africa FSS Beam",
                "alternatename": "Africa",
                "satellite_id": "2149",
                "satellite": "Astra 4A",
                "other_names": "Sirius 4",
                "norad": "32299",
                "real": "4.8",
                "mapcount": 13
            }, {
                "id": "5136",
                "name": "5E-Ku-Europe4",
                "band": "Ku",
                "fullname": "Ku-band Europe BSS Beam",
                "alternatename": "EuropeBSS",
                "satellite_id": "2149",
                "satellite": "Astra 4A",
                "other_names": "Sirius 4",
                "norad": "32299",
                "real": "4.8",
                "mapcount": 13
            }, {
                "id": "6341",
                "name": "5E-Ku-EuropeFSS",
                "band": "Ku",
                "fullname": "Ku-band Europe FSS Beam",
                "alternatename": "EuropeFSS",
                "satellite_id": "2149",
                "satellite": "Astra 4A",
                "other_names": "Sirius 4",
                "norad": "32299",
                "real": "4.8",
                "mapcount": 11
            }, {
                "id": "6461",
                "name": "5E-C-Global",
                "band": "C",
                "fullname": "C-band Global beam",
                "alternatename": "Global",
                "satellite_id": "2488",
                "satellite": "SES 5",
                "other_names": "Sirius 5, Astra 4B",
                "norad": "38652",
                "real": "5",
                "mapcount": 4
            }, {
                "id": "7117",
                "name": "5E-Ka-Interactive",
                "band": "Ka",
                "fullname": "Ka-band Europe Interactive Beam",
                "alternatename": "Interactive",
                "satellite_id": "2149",
                "satellite": "Astra 4A",
                "other_names": "Sirius 4",
                "norad": "32299",
                "real": "4.8",
                "mapcount": 13
            }, {
                "id": "6097",
                "name": "5E-Ka-Europe",
                "band": "Ka",
                "fullname": "Ka-band Europe Interconnect Beam",
                "alternatename": "Interconnect",
                "satellite_id": "2149",
                "satellite": "Astra 4A",
                "other_names": "Sirius 4",
                "norad": "32299",
                "real": "4.8",
                "mapcount": 19
            }, {
                "id": "7143",
                "name": "5E-Ku-Nordic",
                "band": "Ku",
                "fullname": "Ku-band Nordic beam",
                "alternatename": "Nordic",
                "satellite_id": "2488",
                "satellite": "SES 5",
                "other_names": "Sirius 5, Astra 4B",
                "norad": "38652",
                "real": "5",
                "mapcount": 4
            }, {
                "id": "5135",
                "name": "5E-Ku-Nordic4",
                "band": "Ku",
                "fullname": "Ku-band Nordic BSS Beam",
                "alternatename": "NordicBSS",
                "satellite_id": "2149",
                "satellite": "Astra 4A",
                "other_names": "Sirius 4",
                "norad": "32299",
                "real": "4.8",
                "mapcount": 2
            }, {
                "id": "6342",
                "name": "5E-Ku-NordicFSS",
                "band": "Ku",
                "fullname": "Ku-band Nordic FSS Beam",
                "alternatename": "NordicFSS",
                "satellite_id": "2149",
                "satellite": "Astra 4A",
                "other_names": "Sirius 4",
                "norad": "32299",
                "real": "4.8",
                "mapcount": 2
            }, {
                "id": "6460",
                "name": "5E-Ku-SSA",
                "band": "Ku",
                "fullname": "Ku-band SSA beam",
                "alternatename": "SSA",
                "satellite_id": "2488",
                "satellite": "SES 5",
                "other_names": "Sirius 5, Astra 4B",
                "norad": "38652",
                "real": "5",
                "mapcount": 21
            }, {
                "id": "6462",
                "name": "5E-C-WH",
                "band": "C",
                "fullname": "C-band West Hemi beam",
                "alternatename": "WH",
                "satellite_id": "2488",
                "satellite": "SES 5",
                "other_names": "Sirius 5, Astra 4B",
                "norad": "38652",
                "real": "5",
                "mapcount": 10
            }],
            "nomap": false,
            "selected_beam": 0,
            "other_list": [],
            "map_params": {
                "type": "normal",
                "lat": 0,
                "lng": 5,
                "zoom": 1
            }
        }
    }
    '''

class BeamImg(scrapy.Item):
    position = scrapy.Field()
    image_urls = scrapy.Field()
    images = scrapy.Field()
