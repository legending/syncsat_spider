# -*- coding: utf-8 -*-

# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: http://doc.scrapy.org/en/latest/topics/item-pipeline.html

import pymongo
from footprints_spider.items import Footprints,Beam,BeamImg

from scrapy.pipelines.images import ImagesPipeline
from scrapy.exceptions import DropItem
from scrapy.http import Request

class FootprintsSpiderPipeline(ImagesPipeline):
    collection_footprints = 'footprints'
    collection_satbeams = 'beams'
    collection_beam_imgs = 'beamimgs'


    def __init__(self, mongo_uri, mongo_db):
        self.mongo_uri = mongo_uri
        self.mongo_db = mongo_db

    @classmethod
    def from_crawler(cls, crawler):
        return cls(
            mongo_uri=crawler.settings.get('MONGO_URI'),
            mongo_db=crawler.settings.get('MONGO_DATABASE','syncsat')
        )

    def open_spider(self, spider):
        self.client = pymongo.MongoClient(self.mongo_uri)
        self.db = self.client[self.mongo_db]

    def close_spider(self, spider):
        self.client.close()

    def save_footprints(self, item, spider):
        if ('page' not in item.keys() or item['page'] is None or len(item['page']) == 0):
            print('error: ' + str(item))
            return
        footprint = {}
        footprint['page'] = item['page']
        print("save footprints")
        self.db[self.collection_footprints].delete_many({})
        self.db[self.collection_footprints].insert(footprint)  # 添加数据
        self.db[self.collection_footprints].save(footprint)

    def save_beam(self, item, spider):
        if('position' not in item.keys() or item['position'] is None  or len(item['position']) == 0):
            print('error: ' + str(item))
            return
        if('data' not in item.keys() or item['data'] is None  or len(item['data']) == 0):
            print('error: ' + str(item))
            return

        beam = {}
        beam['position'] = item['position']
        beam['data'] = item['data']

        self.db[self.collection_satbeams].update({'position': beam['position']}, {'$set': beam}, upsert=True)

    def save_beam_img(self, item, spider):
        if ('position' not in item.keys() or item['position'] is None or len(item['position']) == 0):
            print('error: ' + str(item))
            return
        beam_img = {}
        beam_img['position'] = item['position']
        beam_img['image_urls'] = item['image_urls']
        beam_img['images'] = item['images']
        self.db[self.collection_beam_imgs].update({'position': beam_img['position']}, {'$set': beam_img},upsert=True)

    def process_item(self, item, spider):
        if isinstance(item, Footprints):
            self.save_footprints(item, spider)
        if isinstance(item, Beam):
            self.save_beam(item, spider)
        if isinstance(item, BeamImg):
            self.save_beam_img(item, spider)
        return item

class DownloadImagesPipeline(ImagesPipeline):
    '''
    def get_media_requests(self, item, info):
        if():
            for image_url in item['image_urls']:
                yield Request(image_url,meta={'item': item, 'index': item['image_urls'].index(image_url)})
    '''

    def file_path(self, request, response=None, info=None):
        '''
        item = request.meta['item']  # 通过上面的meta传递过来item
        index = request.meta['index']  # 通过上面的index传递过来列表中当前下载图片的下标
        image_guid = item['image_urls'][index] + '.' + request.url.split('/')[-1].split('.')[-1]
        filename = u'full/{0}/{1}'.format(item['position'], image_guid)
        return filename
        '''
        image_guid = request.url.split('/')[-1].split(".")[0] + ".jpg"
        print("image: " + image_guid)
        return 'full/%s' % (image_guid)

    def item_completed(self, results, item, info): #将下载成功的图片的路径保存在images字段中
        image_path = [x['path'] for ok, x in results if ok]
        if not image_path:
            raise DropItem('Item contains no images')
        item['images'] = image_path
        return item