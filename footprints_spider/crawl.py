# -*- coding: utf-8 -*-
import json
import requests
from pymongo import MongoClient
import time
import re
import os
import threading
from multiprocessing import Pool, Manager
from bs4 import BeautifulSoup

footprints_url = 'http://satbeams.com/footprints'
img_save_path = os.path.abspath(os.path.join(os.getcwd(), "img"))

requests.adapters.DEFAULT_RETRIES = 5
s = requests.session()
s.keep_alive = False

def getInitData():
    headers = {
        'User-Agent': 'User-Agent:Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36',
    }
    response = requests.get(footprints_url, headers=headers)
    html_txt = response.text
    soup = BeautifulSoup(html_txt, "html.parser")
    init_data_str = soup.find_all("script")[17].get_text()
    init_data_str = init_data_str.split("var data =")[1].strip().strip(";")
    init_data = json.loads(init_data_str)
    del init_data['user_id']
    del init_data['user_data']
    del init_data['not_authorized']
    del init_data['page']['fav']
    #print(init_data)
    client = MongoClient('127.0.0.1', 27017,waitQueueTimeoutMS=10)  # resolve : MongoClient opened before fork. Create MongoClient , so put in process func
    db = client.syncsat
    collection = db.footprints
    collection.delete_many({})
    collection.insert(init_data)
    collection.save(init_data)
    return init_data

def get_footprints_data(init_data):
    positions = init_data['page']['positions']
    #cnt = 0
    time_start = time.time()
    #total_sat_num = 0
    m = Manager()
    d = m.dict()
    d['bean_num'] = 0
    d['total_sat_num'] = 0
    p = Pool(processes = 50)
    for key in positions:
        p.apply_async(get_beams, args=(key,d))
        #get_beams(positions,cnt,time_start,total_sat_num,key)
    p.close()
    p.join()

    time_end = time.time()
    print("beam total " + str(d['bean_num']) + " , " + "sat total " + str(d['total_sat_num']) + " , " + "use " + str(time_end - time_start) + ' s')

def get_beams(key, d):
    client = MongoClient('127.0.0.1', 27017, waitQueueTimeoutMS=10) # resolve : MongoClient opened before fork. Create MongoClient , so put in process func
    db = client.syncsat

    d['bean_num'] += 1

    # if (cnt > 5):
    #    break
    #formdata = {'position': key}
    headers = {
        'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/64.0.3282.186 Safari/537.36'
    }
    #url = "http://satbeams.com/!ajax/footprint?action=get"
    url = "http://satbeams.com/!ajax/footprint?action=get&position=" + key
    #response = requests.get(url, data=formdata, headers=headers)
    response = requests.get(url, headers=headers)
    data = response.text
    data = data.split('<hr />')[0].split('Server return: ')[1]
    data = json.loads(data)
    beam = {'position': key, 'data': data['data']}
    beam_sat_list = data['data']['beams']

    if not os.path.exists(img_save_path):
        os.makedirs(img_save_path)
    for i, item in enumerate(beam_sat_list):
        img_name = item['name'] + '_tn' + '.gif'
        url = 'http://satbeams.com/images/foot_thumbs/' + img_name
        des_path = img_save_path + '/' + img_name
        save_img(url, des_path, img_name)
        #thread = threading.Thread(target=save_img, args=(url, des_path, img_name))
        #thread.start()

    beam_sat_num = len(beam_sat_list)
    d['total_sat_num'] += beam_sat_num
    print("beam " + key + ", " + str(beam_sat_num) + " sat")
    collection = db.beams
    collection.update({'position': key}, {'$set': beam}, upsert=True)
    #collection.insert(beam)
    #collection.save(beam)

def save_img(url, path, img_name):
    headers = {
        'User-Agent': 'User-Agent:Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36',
        'Connection': 'close'
    }
    if(len(img_name)<1):
        return
    r = requests.get(url,headers=headers)
    if(r.status_code == 200):
        img_path = os.path.join(path,img_name)
        if not os.path.exists(img_path):
            with open(path, "wb") as f:
                f.write(r.content)
                print(img_name + " saved")
        else:
            print(img_name + " existed")
    else:
        return

if( __name__ == "__main__"):
    initdata = getInitData()
    get_footprints_data(initdata)

    '''
    positions = init_data['page']['positions']
    i = 0
    for key in positions:
        i += 1
    print(i) #190
    '''

    '''
    #r = requests.get('http://satbeams.com/images/foot_thumbs/116E-Ku-India7_tn.gif') #404
    r = requests.get('http://satbeams.com/images/foot_thumbs/119E-Ku-Spot106_tn.gif') #200
    print(r.status_code)
    '''

    '''
    #print(init_data['page']['positions'])
    ss = r'Server return: {"status":0,"data":"11111111111",''obj":{"aa":12}}ss<hr />ddddd'
    #reg = re.compile(r'. *?')
    #final = re.search(reg,ss).group()
    final = ss.split('<hr />')[0].split('Server return: ')[1]
    print(final)
    '''

