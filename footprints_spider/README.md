####1.python version = 3.5.3
####2.项目历史介绍:
(1)crawl.py不使用任何框架抓取数据,数据库数据正确,但图片下载不全;
(2)为弥补crawl.py图片的不足故新建download_images.py,用的是多线程,但不稳定,图片仍然下载不全;
(3)为彻底解决问题,详细研究了scrapy保存图片原名的问题,最终得到了完整的scrapy,下载比较稳定,图片完整;
####3.运行爬虫:
scrapy crawl satbeams
####4.项目维护:
由于satbeams.com有时候局部调整数据结构,所以需要隔一段时间检查一次,周期大概在2个月
####5.复制图片:
将下载的图片复制到syncsat-client项目的子目录中(先删除原有图片,然后再复制):
syncsat-client/static/footprintsfile/images/foot_thumbs/